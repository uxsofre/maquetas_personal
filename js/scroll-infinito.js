/**
 * Created by Jess on 06/04/2016.
 */
function addScroll() {

    var listNumbersHeight = $('#list-numbers').height();
    var scrollInfinitoHeight = $('.scroll-infinito').height()
    var scrollTopInfinito = $('.scroll-infinito').scrollTop();

    if(listNumbersHeight > scrollInfinitoHeight) {
        if ((listNumbersHeight - scrollInfinitoHeight) >= scrollTopInfinito){
            $('#loading').show();

            $('#list-numbers').append(randomPost());
            $('#loading').hide();
        }
    }
}

// Generate a random post
function randomPost() {

    // Generate the post
    var post1 = '<div class="box-list clearfix">' +
        ' <div class="col-xs-12 col-md-10 col-md-offset-1">' +
        '   <div class="form-group col-xs-12 col-sm-12 col-md-5 col-lg-5 margin-bottom-5">' +
            ' <div class="row">' +
                ' <!---visible solo en xs y md -->' +
                ' <label class="col-xs-4 col-md-2 hidden-md hidden-lg">TIPO</label>' +
                ' <!---visible solo en xs y md -->' +
                '  <div class="col-xs-8 col-md-8">' +
                    '   <p class="margin-top-5"><strong>Télefono Fijo</strong></p>' +
                    ' </div>' +
                ' </div>' +
            ' </div>' +

        '  <div class="form-group col-xs-4 col-sm-4 col-md-1 col-lg-1 margin-bottom-5">' +
            '     <div class="row">' +
                '   <label class="col-xs-3 col-sm-2 col-md-3 col-lg-3">0</label>' +
                '    <div class="col-xs-8 col-sm-9 col-md-7 col-lg-7">' +
                    '        <p class="margin-top-5"><strong>351</strong></p>' +
                    '     </div>' +
                ' </div>' +
                ' </div>' +
            ' <div class="form-group col-xs-5 col-sm-6 col-md-3 col-lg-3 n-celular n-fijo margin-bottom-5">' +
                ' <div class="row">' +
                    '   <label class="col-md-3"> </label>' +
                    '   <div class="col-xs-9 col-sm-9 col-md-9 row">' +
                        '       <p class="margin-top-5"><strong>4511626</strong></p>' +
                        '     </div>' +
                    '   </div>' +
                ' </div>' +

            ' <div class="form-group col-xs-2 col-sm-2 col-md-2 col-lg-2 margin-left-0 margin-bottom-5">' +
                '     <div class="row">' +
       ' <div class="col-xs-5">' +
                    '     <a href="#"><span class="tpicon tpicon-editar"></span></a>' +
                    ' </div>' +
                ' <div class="col-xs-7 row hidden-xs hidden-sm">' +
                    ' <p class="margin-top-5"><a href="#"> <strong>Modificar</strong></a></p>' +
                    ' </div>' +
                '</div>' +
            '</div>' +
        '</div>' +
    '</div>';

    return post1 + post1 + post1 + post1 + post1 + post1+ post1 + post1 + post1
    post1 + post1 + post1 + post1 + post1 + post1+ post1 + post1 + post1
    post1;
}