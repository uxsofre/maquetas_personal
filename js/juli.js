// var card1 = $('div.card-body:first'),
//    card2 = $('div.card-body:last');

//$( "div.card-links:first" ).addClass( "card1" );

//$( "div.card-links:last" ).addClass( "card2" );


jQuery(document).ready( function ($) {

    var mq = window.matchMedia( "(min-width: 992px)" );

    if (mq.matches) {
        var alturaMayor = 0;
        var $divs = $("div.card1-link, div.card2-link");

        $divs.each(function(){
            if ($(this).height() > alturaMayor) {
                alturaMayor = $(this).height();
            }
        });

        $divs.height(alturaMayor);
    }  
});

//$('h1').height('50px');