﻿var _tpi_stamp = new Date().getTime();
var _tpi_stamp_end;
var _tpi_location = document.location;
var _tpi_urlScheme = _tpi_location.protocol;
var _tpi_webres = "//resources.personal.com.ar";
var _tpi_resurl = _tpi_webres +"/components/";
var _tpi_imgurl = _tpi_webres +"/images/";
var _tpi_oAjax;
var _tpi_injectorState = 0;
var _tpi_hasjq = false;
var _tpi_isResponsive = false;
var _tpi_isResponsiveMenuOpen = false;
var _tpi_isSuperMenuOpen = false;
var _tpi_isSearchOpen = false;
var _tpi_isSearchMdOpen = false;
var _tpi_isUserInfoOpen = false;
var _tpi_idleTime = 0;
var _tpi_chatTime = 0;
var _tpi_idleTimer;
var _tpi_chatTimer;
var _tpi_isIdleTimerActive = false;
var _tpi_isChatTimerActive = false;
var _tpi_timeoutChat = 0;
var _tpi_timeoutIdle = 0;
var _tpi_timeoutCountdown = 0;
var _tpi_timeoutCountdownMax = 0;
var _tpi_timeoutCountdownActive = false;
var _tpi_timeoutMenu;

if (typeof jQuery !== 'undefined') {
  (function($) {
    $.versioncompare = function(version1, version2) {
      if ('undefined' === typeof version1) {
        throw new Error("$.versioncompare needs at least one parameter.");
      }
      version2 = version2 || $.fn.jquery;
      if (version1 == version2) {
        return 0;
      }
      var v1 = normalize(version1);
      var v2 = normalize(version2);
      var len = Math.max(v1.length, v2.length);
      for (var i = 0; i < len; i++) {
        v1[i] = v1[i] || 0;
        v2[i] = v2[i] || 0;
        if (v1[i] == v2[i]) {
          continue;
        }
        return v1[i] > v2[i] ? 1 : -1;
      }
      return 0;
    };
    function normalize(version){
      return $.map(version.split('.'), function(value){
        return parseInt(value, 10);
      });
    }
    var _version_check = $.versioncompare($.fn.jquery, "1.7.0");
    _tpi_hasjq = _version_check >= 0;
    if (_tpi_hasjq && typeof $.fn.hoverIntent == 'undefined') {
      $.fn.hoverIntent=function(handlerIn,handlerOut,selector){var cfg={interval:100,sensitivity:6,timeout:0};if(typeof handlerIn==="object"){cfg=$.extend(cfg,handlerIn)}else{if($.isFunction(handlerOut)){cfg=$.extend(cfg,{over:handlerIn,out:handlerOut,selector:selector})}else{cfg=$.extend(cfg,{over:handlerIn,out:handlerIn,selector:handlerOut})}}var cX,cY,pX,pY;var track=function(ev){cX=ev.pageX;cY=ev.pageY};var compare=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);if(Math.sqrt((pX-cX)*(pX-cX)+(pY-cY)*(pY-cY))<cfg.sensitivity){$(ob).off("mousemove.hoverIntent",track);ob.hoverIntent_s=true;return cfg.over.apply(ob,[ev])}else{pX=cX;pY=cY;ob.hoverIntent_t=setTimeout(function(){compare(ev,ob)},cfg.interval)}};var delay=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);ob.hoverIntent_s=false;return cfg.out.apply(ob,[ev])};var handleHover=function(e){var ev=$.extend({},e);var ob=this;if(ob.hoverIntent_t){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t)}if(e.type==="mouseenter"){pX=ev.pageX;pY=ev.pageY;$(ob).on("mousemove.hoverIntent",track);if(!ob.hoverIntent_s){ob.hoverIntent_t=setTimeout(function(){compare(ev,ob)},cfg.interval)}}else{$(ob).off("mousemove.hoverIntent",track);if(ob.hoverIntent_s){ob.hoverIntent_t=setTimeout(function(){delay(ev,ob)},cfg.timeout)}}};return this.on({"mouseenter.hoverIntent":handleHover,"mouseleave.hoverIntent":handleHover},cfg.selector)};
    }
  })(jQuery);
}

function _tpi_getAjax() {
  var oXHR = null;
  if (window.ActiveXObject) {
    oXHR = new ActiveXObject("Microsoft.XMLHTTP");
  } else {
    if (window.XMLHttpRequest) {
      oXHR = new XMLHttpRequest();
    }
  }
  return oXHR;
}

function _tpi_addEvent(oObj, sEvent, sFunction, bCapture) {
  if (oObj.attachEvent) {
    oObj.attachEvent('on' + sEvent, sFunction);
    return true;
  } else if (oObj.addEventListener) {
    oObj.addEventListener(sEvent, sFunction, bCapture);
    return true;
  } else {
    return false;
  }
}

function _tpi_addRemoveClass(oObj, sClass, isAdd) {
  if(oObj){
    if (oObj.classList) {
      if (isAdd) {
        oObj.classList.add(sClass);
      } else {
        oObj.classList.remove(sClass);
      }
    } else {
      if (isAdd) {
        oObj.className += ' ' + sClass;
      } else {
        oObj.className = oObj.className.replace(new RegExp('(^|\\b)' + sClass.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
      }
    }
  }
}

function _tpi_hasClass(oObj, sClass) {
  if (oObj) {
    return oObj.className.match(new RegExp('(\\s|^)'+sClass+'(\\s|$)'));
  }
}

function _tpi_insertAfter(newNode, referenceNode) {
  referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

function _tpi_funcTraverseChilds(oObj) {
  var oObjChilds = [];
  var oQueue = [];
  oQueue.push(oObj);
  while (oQueue.length > 0) {
    var oObj = oQueue.pop();
    oObjChilds.push(oObj);
    pushAll(oObj.children);
  }
  function pushAll(oObjArray){
    for(var i = 0; i < oObjArray.length; i++) {
      oQueue.push(oObjArray[i]);
    }
  }
  return oObjChilds;
}

function _tpi_funcStampUrl(sUrl) {
  return sUrl + (sUrl.indexOf('?') != -1 ? "&v=" : "?v=") + String(_tpi_stamp).substring(String(_tpi_stamp).length, String(_tpi_stamp).length - 6);
}

function _tpi_funcHasStorage() {
  try {
    return 'localStorage' in window && window['localStorage'] !== null;
  } catch (e) {
    return false;
  }
}

function _tpi_funcCapitalise(sText) {
  return sText.toLowerCase().replace(/^(.)|\s(.)/g, function($1) {
    return $1.toUpperCase(); 
  });
}

function _tpi_statsComScore(sTag) {
  var t = 'http' + (_tpi_location.href.charAt(4) == 's' ? 's://sb' : '://b') + '.scorecardresearch.com/p?c1=2&c2=6906383&ns_site=personal&name='+sTag;
  var b = "comScore", o = document, f = _tpi_location, a = "", e = "undefined", g = 2048, s, k, p, h, r = "characterSet", n = "defaultCharset", m = (typeof encodeURIComponent != e ? encodeURIComponent : escape);
  if (o.cookie.indexOf(b + "=") != -1) {
      p = o.cookie.split(";");
      for (h = 0, f = p.length; h < f; h++) {
          var q = p[h].indexOf(b + "=");
          if (q != -1) {
              a = "&" + unescape(p[h].substring(q + b.length + 1))
          }
      }
  }
  t = t + "&ns__t=" + (new Date().getTime());
  t = t + "&ns_c=" + (o[r] ? o[r] : (o[n] ? o[n] : "")) + "&c8=" + m(o.title) + a + "&c7=" + m(f && f.href ? f.href : o.URL) + "&c9=" + m(o.referrer);
  if (t.length > g && t.indexOf("&") > 0) {
      s = t.substr(0, g - 8).lastIndexOf("&");
      t = (t.substring(0, s) + "&ns_cut=" + m(t.substring(s + 1))).substr(0, g)
  }
  if (o.images) {
      k = new Image();
      if (typeof ns_p == e) {
          ns_p = k
      }
      k.src = t
  } else {
      o.write('<p><' + 'img src="' + t + '" height="1" width="1" alt="*"></p>')
  }
}

function _tpi_statsGTM(w,d,s,l,i) {w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);}

function _tpi_getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i=0; i<ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1);
    if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
  }
  return "";
}

if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function(elt) {
    var len = this.length >>> 0;
    var from = Number(arguments[1]) || 0;
    from = (from < 0) ? Math.ceil(from) : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++) {
      if (from in this &&
        this[from] === elt)
        return from;
    }
    return -1;
  };
}

if (!String.prototype.startsWith) {
  String.prototype.startsWith = function (str) {
      return !this.indexOf(str);
  }
}

$.fn.center = function() {
  this.css({'position': 'fixed','left': '50%','top': '50%'});
  this.css({'margin-left': -this.outerWidth() / 2 + 'px','margin-top': -this.outerHeight() / 2 + 'px'});
  return this;
}

var oTPInjector = {
  getUrlVars: function () {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
      hash = hashes[i].split('=');
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
    }
    return vars;
  },
  getUrlVar: function (name) {
    return oTPInjector.getUrlVars()[name];
  },
  user: {
    login: false,
    name: '',
    email: '',
    dni: '',
    line: '',
    club: false,
    puntos: 0,
    plan: '',
    segmento:'',
    subsegmento:'',
    isFactura: false,
    isAbonoFijo: false,
    isTarjeta: false
  },
  env: {
    loadtime: null,
    urlhash: _tpi_location.hash,
    angular: false
  },
  config: {
    logospos: [0, -43, -86, -129, -172, -215, -258, -301, -344],
    scrollTrigger: 32,
    marginTopDesktop: '122px',
    marginTopMobile: '48px',
    superMenuWait: 300,
    superMenuShowDelay: 200,
    superMenuHideDelay: 200,
    responsiveWidth: 942,
    responsiveMenuWidth: -300,
    responsiveMenuDelay: 200,
    hasHeader: true,
    hasFooter: true,
    hasLogin: true,
    hasLoggedin: true,
    hasSearch: true,
    hasChat: false,
    appName: '',
    appSection: '',
    id: function () {
      return this.company + '.' + this.headerType;
    },
    company: 'personal',
    headerType: 'individuos',
    theme: 'default',
    menuItem: 0,
    urlParams: function (type) {
      if (type == 'header') {
        return "?hasHeader=" + this.hasHeader + "&headerType=" + this.headerType + "&hasLogin=" + this.hasLogin + "&hasLoggedin=" + this.hasLoggedin + "&hasSearch=" + this.hasSearch;
      } else {
        return "?hasFooter=" + this.hasFooter + "&footerType=" + this.headerType;
      }
    },
    siteStyle: function (baseUrl) {
      return _tpi_funcStampUrl(baseUrl + 'tpinjector/v1/theme_' + oTPInjector.config.company + '_' + oTPInjector.config.headerType + '_' + oTPInjector.config.theme + '.css');
    },
    oficina: '',
    urlLogout: '',
    urlRelogin: ''
  }
};

(function(){
  _tpi_chanchullos(true);

  if (_tpi_funcHasStorage()) {
    for(var i=0, len=localStorage.length; i<len; i++) {
      var key = localStorage.key(i);
      var value = localStorage[key];

      if(key && key.indexOf("typeahead/v2/") > -1){
        localStorage.removeItem(key);
      }
    }
  }

  var oElement;
  // oElement = div header
  oElement = document.createElement('div');
  oElement.id = '_injector_header';
  oElement.innerHTML = '<img id="_injector_header_loading" src="' + _tpi_imgurl + 'gif/ajax-loader.gif">';
  document.body.insertBefore(oElement, document.body.firstChild);
  //Mensaje de advertencia IE8
  if (_tpi_checkIEVersion()) {
    var oElement = document.createElement('div');
    oElement.id = '_injector_msj';
    var sInnerHTML = "<div class='tpi-browser-warning'>";
    sInnerHTML += "<div class='tpi-browser-warning-msg'><strong>Tu navegador no est&aacute; actualizado.</strong> Descarg&aacute; gratis la nueva versi&oacute;n</div>";
    sInnerHTML += "<a href='http://windows.microsoft.com/es-es/internet-explorer/download-ie' target='_blank' class='icon-ie'>Internet Explorer</a>";
    sInnerHTML += "<a href='http://www.google.com.ar/intl/es-419/chrome/' target='_blank' class='icon-chrome'>Chrome</a>";
    sInnerHTML += "<a href='https://www.mozilla.org/es-AR/firefox/new/' target='_blank' class='icon-firefox'>Firefox</a>";
    sInnerHTML += "<a href='http://www.opera.com/es-419' target='_blank' class='icon-opera'>Opera</a>";
    sInnerHTML += "<a href='http://support.apple.com/kb/dl1531' target='_blank' class='icon-safari'>Safari</a>";
    sInnerHTML += "</div>";
    oElement.innerHTML = sInnerHTML;
    document.body.insertBefore(oElement, document.body.firstChild);
  }
  // oElement = div footer
  oElement = document.createElement('div');
  oElement.id = '_injector_footer';
  //document.body.appendChild(oElement);
  document.body.insertBefore(oElement, null);
  // oElement = css injector
  oElement = document.createElement('link');
  oElement.type = 'text/css';
  oElement.href = _tpi_funcStampUrl(_tpi_resurl + 'tpinjector/v1/tpinjector.css');
  oElement.rel = 'stylesheet';
  document.body.appendChild(oElement);
  // oElement = div config
  oElement = document.createElement('div');
  oElement.id = '_injector_config';
  document.body.insertBefore(oElement, document.body.firstChild);
  _tpi_loadConfig();
  _tpi_addEvent(window, 'load', _tpi_winloadCallback, false);
  _tpi_addEvent(window, 'resize', _tpi_responsiveCallback, false);
})();

function _tpi_loadConfig() {
  var sReferrer = document.referrer || '';
  var oElement = document.getElementById('_injector_config');
  var sURL = _tpi_funcStampUrl(_tpi_resurl + 'tpinjector/v1/tpinjector_config.aspx?clReferrer='+sReferrer+'&tpi-segmento='+oTPInjector.getUrlVar('tpi-segmento'));
  var oLocalAjax = _tpi_getAjax();
  oLocalAjax.open("GET", sURL, true);
  if (oLocalAjax.withCredentials !== undefined) {
    oLocalAjax.withCredentials = true;
  }
  oLocalAjax.onreadystatechange = function () {
    if (oLocalAjax.readyState == 4) {
      oElement.innerHTML = oLocalAjax.responseText;
      oTPInjector.user.login = document.getElementById('tpi_ssoUserLine').value != '';
      oTPInjector.user.name = document.getElementById('tpi_ssoUserName').value;
      oTPInjector.user.email = document.getElementById('tpi_ssoUserEmail').value;
      oTPInjector.user.dni = document.getElementById('tpi_ssoUserDNI').value;
      oTPInjector.user.line = document.getElementById('tpi_ssoUserLine').value;
      oTPInjector.user.club = document.getElementById('tpi_ssoIsUserClub').value == 'true';
      oTPInjector.user.puntos = oTPInjector.user.club ? parseInt(document.getElementById('tpi_ssoUserClubPuntos').value, 10) : 0;
      oTPInjector.user.plan = document.getElementById('tpi_ssoUserTipoPlan').value;
      oTPInjector.user.segmento = document.getElementById('tpi_ssoUserLineaSegmento').value;
      oTPInjector.user.subsegmento = document.getElementById('tpi_ssoUserLineaSubSegmento').value;
      oTPInjector.user.isFactura = document.getElementById('tpi_ssoIsLineFactura').value == 'true';
      oTPInjector.user.isAbonoFijo = document.getElementById('tpi_ssoIsLineAbonoFijo').value == 'true';
      oTPInjector.user.isTarjeta = document.getElementById('tpi_ssoIsLineTarjeta').value == 'true';
      oTPInjector.config.hasHeader = document.getElementById('tpi_hasHeader').value == 'true';
      oTPInjector.config.hasFooter = document.getElementById('tpi_hasFooter').value == 'true';
      oTPInjector.config.hasLogin = document.getElementById('tpi_hasLogin').value == 'true';
      oTPInjector.config.hasLoggedin = document.getElementById('tpi_hasLoggedin').value == 'true';
      oTPInjector.config.hasSearch = _tpi_checkIEVersion() ? false : document.getElementById('tpi_hasSearch').value == 'true';
      oTPInjector.config.hasChat = document.getElementById('tpi_hasChat').value == 'true';
      oTPInjector.config.company = document.getElementById('tpi_company').value;
      if (oTPInjector.config.company == "oficinacomercial") {
        oTPInjector.config.marginTopDesktop = "72px";
        oTPInjector.config.oficina = _tpi_getCookie("tpinjector_oficina");
      }
      oTPInjector.config.headerType = document.getElementById('tpi_headerType').value != '' ? document.getElementById('tpi_headerType').value : 'individuos';
      oTPInjector.config.theme = document.getElementById('tpi_theme').value;
      oTPInjector.config.menuItem = parseInt(document.getElementById('tpi_menuItem').value, 10);
      oTPInjector.config.appName = document.getElementById('tpi_appName').value;
      oTPInjector.config.appSection = document.getElementById('tpi_appSection').value;
      oTPInjector.config.urlLogout = document.getElementById('tpi_urlLogout').value;
      oTPInjector.config.urlRelogin = document.getElementById('tpi_urlRelogin').value;
      document.body.style.marginTop = oTPInjector.config.marginTopDesktop;
      oElement = document.createElement('link');
      oElement.type = 'text/css';
      oElement.href = oTPInjector.config.siteStyle(_tpi_resurl);
      oElement.rel = 'stylesheet';
      document.body.appendChild(oElement);
      _tpi_getAjaxContent();
    }
  };
  oLocalAjax.send(null);
}

function _tpi_getAjaxContent() {
  if (_tpi_injectorState == 0) {
    _tpi_loadTypeahead();
  }

  var sObj = _tpi_injectorState == 0 ? 'header' : 'footer';
  var oElement = document.getElementById('_injector_' + sObj);
  var sURL = _tpi_funcStampUrl(_tpi_resurl + 'tpinjector/v1/tpinjector_' + sObj + '_' + oTPInjector.config.company + '.aspx' + oTPInjector.config.urlParams(sObj));
  oAjax = _tpi_getAjax();
  oAjax.open("GET", sURL, true);
  if (oAjax.withCredentials !== undefined) {
    oAjax.withCredentials = true;
  }
  oAjax.onreadystatechange = _tpi_getAjaxContentReady;
  oAjax.send(null);
}

function _tpi_getAjaxContentReady() {
  var oElement = document.getElementById('_injector_' + (_tpi_injectorState == 0 ? 'header' : 'footer'));
  if (oAjax.readyState == 4) {
    oElement.innerHTML = oAjax.responseText;
    if (_tpi_injectorState == 0) {
      oAjax = null;
      _tpi_injectorState = 1;
      _tpi_getAjaxContent();
      return false;
    }
    if (_tpi_injectorState == 1) {
      _tpi_initEventListeners();
      _tpi_responsiveCallback();
      return false;
    }
  }
}

function _tpi_initEventListeners() {
  oTPInjector.env.angular = typeof angular !== 'undefined';
  _tpi_initTPBanner();
  _tpi_addEvent(window, 'scroll', _tpi_scrollCallback, false);
  if (oTPInjector.config.hasHeader) {
    /* Perdon MJ, me obligaron */
    var oBody = document.body;
    oBody.onclick = function (event) {
      if (!_tpi_timeoutCountdownActive) _tpi_resetIdleTime();
      event = event || window.event;
      var targetElement = event.target || event.srcElement;
      var sId = targetElement.id;
      if (sId !== 'tpi-channel-group' && sId !== 'tpi-channel-business' && sId !== 'tpi-channel-menu-trigger') {
        _tpi_HideChannelsMenus();
      }
      if (sId !== 'tpi-user' && _tpi_isUserInfoOpen) {
        document.getElementById('tpi-user-info').style.display = 'none';
        document.getElementById('tpi-icon-deco-user').style.display = 'none';
      }
    };
    _tpi_togglePreheaderMenu('tpi-preheader-mobile-label');
    _tpi_toggleChannelMenu('tpi-channel-group');
    _tpi_toggleChannelMenu('tpi-channel-business');
    _tpi_toggleUserMenu();
    var oMenuBtn = document.getElementById("tpi-menu-btn");
    if (oMenuBtn) {
      oMenuBtn.onclick = function(){_tpi_toggleMenu();};
    }

    if (oTPInjector.env.angular && oTPInjector.config.hasLogin && !oTPInjector.user.login) {
      //document.getElementById('tpi-login').href += oTPInjector.env.urlhash;
    }

    if (oTPInjector.config.hasLoggedin || oTPInjector.config.company == "oficinacomercial") {
      var oBtnLogOff = document.getElementById("tpi-form-logoff");
      if (oBtnLogOff) oBtnLogOff.onclick = function(){ _tpi_form_logoff(); };
      var oUserBtn = document.getElementById("tpi-user-btn-sx");
      if (oUserBtn) oUserBtn.onclick = function(){ _tpi_toggleUserInfo(); };
      var oBtnMoreLinesShow = document.getElementById("tpi-morelines-show");
      if (oBtnMoreLinesShow) oBtnMoreLinesShow.onclick = function(){ _tpi_toggleMoreLinesModal(true); };
    }
    if (oTPInjector.config.hasHeader && oTPInjector.config.hasSearch) {
      var oSearchInput = document.getElementById("searchable1");
      if (oTPInjector.config.menuItem != 2){
        oSearchInput.onfocus = function(){
          this.placeholder = '';
        };
        oSearchInput.onblur = function(){
          this.placeholder = "\u00bfQu\u00e9 queres hacer?";
        };
      }
      var oSearchBtn = document.getElementById("tpi-search-btn");
      oSearchBtn.onclick = function () { _tpi_toggleSearch(); };
      var oSearchBtnMd= document.getElementById("tpi-search-btn-md");
      oSearchBtnMd.onclick = function () { _tpi_toggleSearchMd(); };
    }

    if (!_tpi_hasjq) {
      var oNavbarItems = document.querySelectorAll(".tpi-navbar-item");
      var oNavbarBg = document.getElementById("tpi-navbar-inner-bg");
      for (var i = 0; i < oNavbarItems.length; i++) {
        if (!_tpi_hasClass(oNavbarItems[i], 'tpi-navbar-empty')) {
          oNavbarItems[i].onmouseover = function () {
            if (!_tpi_isSuperMenuOpen) {
              document.querySelectorAll
              _tpi_timeoutMenu = window.setTimeout("_tpi_toggleSuperMenu('" + this.id + "', true)", 350);
            }
          };
          _tpi_addEvent(oNavbarItems[i], 'mouseout', _tpi_HookSuperMenu(oNavbarItems[i]), true);
        }
      }
    }

    if (oTPInjector.config.menuItem > 0) {
      _tpi_addRemoveClass(document.getElementById('tpi-navbar-item' + (oTPInjector.config.menuItem)), 'tpi-navbar-item-active', true);
    }
    if (oTPInjector.config.company == 'personal') {
      document.getElementById("tpi-logo").style.backgroundPosition = '0 ' + oTPInjector.config.logospos[Math.floor(Math.random() * oTPInjector.config.logospos.length)] + 'px';
    }
    $('.tpi-relogin-link').on('click', function(){
      _tpi_form_relogin(this.innerText);
    });
  }

  _tpi_showMsg(oTPInjector.getUrlVar('tpi-msgtext'));
  _tpi_initTimeoutLogin(oTPInjector.config.company == "oficinacomercial");
  _tpi_initTimeoutChat(oTPInjector.config.hasChat);
  _tpi_chanchullos(false);
}

function _tpi_initTPBanner() {
  var oTPBannerContainer = document.getElementById("tpi-banner");
  if (oTPBannerContainer != null ) {
    var oElement = document.createElement('script');
    oElement.type = 'text/javascript';
    oElement.src = _tpi_resurl + 'tpbanner/tpbanner.js';
    document.body.appendChild(oElement);
  }
}

function _tpi_responsiveCallback() {
  if (oTPInjector.config.hasHeader) {
    _tpi_isResponsive = window.innerWidth <= oTPInjector.config.responsiveWidth;
    var oUser = document.getElementById("tpi-user-context");
    var oHeader = document.getElementById("tpi-header");
    var oHeaderPre = document.getElementById("tpi-preheader");
    if (_tpi_isResponsive) {
      if (oHeaderPre) {
        oHeaderPre.style.display = 'block';
      }
      if (oHeader) {
        oHeader.style.position = 'fixed';
        document.body.style.marginTop = oTPInjector.config.marginTopMobile;
        if (!_tpi_isResponsiveMenuOpen) document.getElementById('tpi-header').style.left = oTPInjector.config.responsiveMenuWidth + 'px';
        if ((oTPInjector.config.hasLogin || oTPInjector.config.hasLoggedin) && oUser.parentNode.id != 'tpi-user-sx') document.getElementById("tpi-user-sx").appendChild(oUser);
        if (_tpi_hasjq) {
          $('.tpi-navbar-item').unbind("mouseenter").unbind("mouseleave");
          $('.tpi-navbar-item').removeProp('hoverIntent_t');
          $('.tpi-navbar-item').removeProp('hoverIntent_s');
          $('.tpi-navbar-inner').show();
        }
      }
    } else {
      document.body.style.marginTop = oTPInjector.config.marginTopDesktop;
      _tpi_scrollCallback();
      if (_tpi_isResponsiveMenuOpen) _tpi_toggleMenu(false);
      if (_tpi_isSearchOpen) _tpi_toggleSearch(false);
      if (_tpi_isSearchMdOpen) _tpi_toggleSearchMd(false);
      if (_tpi_isUserInfoOpen) _tpi_toggleUserInfo(false);
      if (oHeader) {
        oHeader.style.left = 0;
        if ((oTPInjector.config.hasLogin || oTPInjector.config.hasLoggedin) && oUser.parentNode.id != 'tpi-user') document.getElementById("tpi-user").appendChild(oUser);
      }
      if (_tpi_hasjq) {
        $('.tpi-navbar-inner').hide();
        $('.tpi-navbar-item').hoverIntent({over: _tpi_toggleSuperMenuJQ, out: _tpi_toggleSuperMenuJQ, timeout: oTPInjector.config.superMenuWait});
      }
    }
  }
}

//if jQuery > Typeahead
function _tpi_loadTypeahead() {
  // oElement = css typeahead
  var oHandlebars;
  var oInit;
  var oElement = document.createElement('link');
  oElement.type = 'text/css';
  oElement.href = _tpi_resurl + 'typeahead/v2/typeahead.css';
  oElement.rel = 'stylesheet';
  document.body.appendChild(oElement);
  if (_tpi_hasjq) {
    // oElement = js typeahead
    oElement = document.createElement('script');
    oElement.type = 'text/javascript';
    document.body.appendChild(oElement);
    oElement.onload = function() {
      // oHandelbars = js handlebars
      oHandlebars = document.createElement('script');
      oHandlebars.type = 'text/javascript';
      document.body.appendChild(oHandlebars);
      oHandlebars.onload = function() {
          //oInit = js typeahead init
          oInit = document.createElement('script');
          oInit.type = 'text/javascript';
          document.body.appendChild(oInit);
          oInit.onload = function() {
            setTimeout(function() {
              _tpi_initTypeahead();
            }, 1000);
          }; //fin onload oInit
          oInit.src = _tpi_resurl + 'typeahead/v2/typeahead-init.js';
      }; //fin onload oHandelbars
      oHandlebars.src = _tpi_resurl + 'typeahead/v2/handlebars.js';
    }; //fin onload oElement
    oElement.src = _tpi_resurl + 'typeahead/v2/typeahead.bundle.min.js';
  } //fin if tpi_hasjq
}

function _tpi_scrollCallback() {
  if (!_tpi_timeoutCountdownActive) _tpi_resetIdleTime();
  if (oTPInjector.config.hasHeader) {
    var oHeader = document.getElementById("tpi-header");
    var oSearch = document.getElementById("tpi-search-sx");
    if (!_tpi_isResponsive) {
      var oHeaderPre = document.getElementById("tpi-preheader");
      var iValueY = window.scrollY || window.pageYOffset;
      if (iValueY < oTPInjector.config.scrollTrigger) {
        _tpi_addRemoveClass(oHeader, 'tpi-header-scrollin', false);
        _tpi_addRemoveClass(oHeader, 'tpi-header-scrollout', true);
        if (oTPInjector.config.hasSearch) {
          oSearch.style.position = 'absolute';
          oSearch.style.top = oTPInjector.config.marginTopDesktop;
        }
        if (oHeaderPre) {
          oHeaderPre.style.display = 'block';
        }
      }
      if (iValueY >= oTPInjector.config.scrollTrigger) {
        _tpi_addRemoveClass(oHeader, 'tpi-header-scrollout', false);
        _tpi_addRemoveClass(oHeader, 'tpi-header-scrollin', true);
        if (oTPInjector.config.hasSearch) {
          oSearch.style.position = 'fixed';
          oSearch.style.top = oTPInjector.config.marginTopMobile;
        }
        if (oHeaderPre) {
          oHeaderPre.style.display = 'none';
        }
      }
    } else {
      if (oHeader) {
        oHeader.style.position = 'fixed';
      }
      if (oTPInjector.config.hasSearch) {
        oSearch.style.position = 'fixed';
      }
    }
  }
}

function _tpi_togglePreheaderMenu(sObjId) {
  var oObj = document.getElementById(sObjId);
  if (oObj) {
    oObj.onclick = function() {
      var oPreheaderMenu = document.getElementById('tpi-preheader-section-links');
      oPreheaderMenu.style.display = oPreheaderMenu.style.display == 'block' ? 'none' : 'block';
    }
  }
}

function _tpi_toggleChannelMenu(sObjId) {
  var oObj = document.getElementById(sObjId);
  if (oObj) {
    oObj.onclick = function() {
      var oGroupMenu = document.getElementById('tpi-channel-group-menu');
      var oBusinessMenu = document.getElementById('tpi-channel-business-menu');
      if (oGroupMenu && oGroupMenu.style.display != 'none' && sObjId != 'tpi-channel-group') {
        oGroupMenu.style.display = 'none';
        _tpi_addRemoveClass(document.getElementById('tpi-channel-group'), 'active', false);
      }
      if (oBusinessMenu && oBusinessMenu.style.display != 'none' && sObjId != 'tpi-channel-business') {
        oBusinessMenu.style.display = 'none';
        _tpi_addRemoveClass(document.getElementById('tpi-channel-business'), 'active', false);
      }
      var oSubMenu = document.getElementById(sObjId+'-menu');
      var bActive = oSubMenu.style.display == 'block';
      _tpi_addRemoveClass(oObj, 'active', !bActive);
      oSubMenu.style.display = !bActive ? 'block' : 'none';
    };
  }
}

function _tpi_HideChannelsMenus() {
  var channelmenus = document.getElementById('tpi-channel-group-menu');
  if(channelmenus){
    channelmenus.style.display = 'none'; 
    _tpi_addRemoveClass(document.getElementById('tpi-channel-group'), 'active', false);
    if (document.getElementById('tpi-channel-business-menu')) {
      document.getElementById('tpi-channel-business-menu').style.display = 'none';
      _tpi_addRemoveClass(document.getElementById('tpi-channel-business'), 'active', false);
    } 
  }
}

function _tpi_toggleSuperMenuJQ(event) {
  if (!event) event = window.event;
  var oMenu = this;
  if (!$(oMenu).hasClass('tpi-navbar-empty')) {
    var oBg = "." + oMenu.id + "-bg";
    if (event.type == 'mouseenter') {
      $(oBg).fadeIn(oTPInjector.config.superMenuShowDelay, function() {
        $(oMenu).find('ul.tpi-navbar-inner').fadeIn(oTPInjector.config.superMenuShowDelay);
      });
    }
    if (event.type == 'mouseleave') {
      $(oMenu).find('ul.tpi-navbar-inner').fadeOut(oTPInjector.config.superMenuHideDelay, function(){
        $(oBg).fadeOut(oTPInjector.config.superMenuHideDelay);  
      });
    }
  }
}

function _tpi_toggleSuperMenu(oMenuId, isShow) {
  if (!_tpi_isResponsive) {
    var oNavbarBg = document.getElementById("tpi-navbar-inner-bg");
    var oNavbarItems = document.querySelectorAll(".tpi-navbar-item");
    for (var i = 0; i < oNavbarItems.length; i++) {
      if (!_tpi_hasClass(oNavbarItems[i], 'tpi-navbar-empty')) {
        document.getElementById(oNavbarItems[i].id + "-content").style.display = "none";
        oNavbarBg.style.display = "none";
        _tpi_isSuperMenuOpen = false;
      }
    }
    if (isShow) {
      document.getElementById(oMenuId + "-content").style.display = "block";
      oNavbarBg.style.display = "block";
      _tpi_isSuperMenuOpen = true;
    }
  }
}

function _tpi_HookSuperMenu(oMenu) {
  var oList = _tpi_funcTraverseChilds(oMenu);
  return function onMouseOut(event) {
    event = event || window.event;
    var e = event.toElement || event.relatedTarget;
    if (!!~oList.indexOf(e)) {
      return;
    }
    window.clearTimeout(_tpi_timeoutMenu);
    _tpi_toggleSuperMenu(oMenu.id, false);
  };
}

function _tpi_toggleMenu(show) {
  var oBody = document.body;
  var oHeader = document.getElementById('tpi-header');
  var oMenuBg = document.getElementById('tpi-background');
  var isShow = show !== undefined ? show : (oHeader.style.left != '0px' || oHeader.style.left == '');
  if (isShow) {
    if (oTPInjector.config.hasSearch && _tpi_isSearchOpen) _tpi_toggleSearch(false);
    if (oTPInjector.config.hasLoggedin && _tpi_isUserInfoOpen) _tpi_toggleUserInfo(false);
    _tpi_isResponsiveMenuOpen = true;
    move(oHeader, circEaseOut, oTPInjector.config.responsiveMenuDelay, oTPInjector.config.responsiveMenuWidth);
    oMenuBg.style.backgroundColor = 'rgba(0, 0, 0, 0.7)';
    oMenuBg.style.display = 'block';
    oMenuBg.onclick = function() {
      _tpi_toggleMenu();
    };
    oBody.style.overflow = 'hidden';
    oMenuBg.addEventListener("touchmove", function(event) {
      event.preventDefault();
      return false;
    }, false);
  } else {
    _tpi_isResponsiveMenuOpen = false;
    if (_tpi_isResponsive) {
      move(oHeader, circInvEaseOut, oTPInjector.config.responsiveMenuDelay, oTPInjector.config.responsiveMenuWidth);
    } else {
      oHeader.style.left = 0;
    }
    oMenuBg.style.display = 'none';
    oBody.style.overflow = 'auto';
  }
  _tpi_addRemoveClass(document.getElementById('tpi-menu-btn'), 'tpi-menu-open', _tpi_isResponsiveMenuOpen);
}

function _tpi_toggleSearch(show) {
  if (oTPInjector.config.hasSearch) {
    var oSearch = document.getElementById('tpi-search-sx');
    var oSearchInput = document.getElementById('searchable-sx');
    var oIcon = document.getElementById('tpi-icon-search');
    var isShow = show !== undefined ? show : (oSearch.style.display == 'none' || oSearch.style.display == '');
    if (isShow) {
      if (_tpi_isResponsiveMenuOpen) _tpi_toggleMenu(false);
      if (_tpi_isUserInfoOpen) _tpi_toggleUserInfo(false);
      _tpi_isSearchOpen = true;
      oSearch.style.top = '48px';
      oSearch.style.display = 'block';
      oSearchInput.focus();
      oIcon.style.display = 'block';
    } else {
      _tpi_isSearchOpen = false;
      oSearch.style.display = 'none';  
      oIcon.style.display = 'none';
    }
  }
}

function _tpi_toggleSearchMd(show) {
  if (oTPInjector.config.hasSearch) {
    var oSearch = document.getElementById('tpi-search-sx');
    var oSearchInput = document.getElementById('searchable-sx');
    var oIcon = document.getElementById('tpi-icon-search2');
    var isShow = show !== undefined ? show : (oSearch.style.display == 'none' || oSearch.style.display == '');
    if (isShow) {
      _tpi_isSearchMdOpen = true;
      oSearch.style.display = 'block';
      oSearchInput.focus();
      oIcon.style.display = 'block';
    } else {
      _tpi_isSearchMdOpen = false;
      oSearch.style.display = 'none'; 
      oIcon.style.display = 'none'; 
    }
  }
}

function _tpi_toggleUserInfo(show) {
  if (oTPInjector.config.hasLoggedin) {
    var oPanel = document.getElementById('tpi-user-info');
    var oMenuBg = document.getElementById('tpi-background');
    var isShow = show !== undefined ? show : (oPanel.style.display == 'none' || oPanel.style.display == '');
    if (isShow) {
      if (_tpi_isResponsiveMenuOpen) _tpi_toggleMenu(false);
      if (_tpi_isSearchOpen) _tpi_toggleSearch(false);
      _tpi_isUserInfoOpen = true;
      oMenuBg.style.backgroundColor = '#ffffff';
      oMenuBg.style.display = 'block';
      oPanel.style.display = 'block';
      oMenuBg.onclick = function(){_tpi_toggleUserInfo();};
      document.body.style.overflow = 'hidden';
    } else {
      _tpi_isUserInfoOpen = false;
      oPanel.style.display = '';
      oMenuBg.style.display = 'none';
      document.body.style.overflow = 'auto';
    }
    var e = event || window.event;
    e.cancelBubble = true;
    if (e.stopPropagation) e.stopPropagation();
  }
}

function _tpi_toggleUserMenu() {
  var oObj = document.getElementById('tpi-user');
  var oMenu = document.getElementById('tpi-user-info');
  if (oMenu == null) return;
  var oIcon = document.getElementById('tpi-icon-deco-user');
  oObj.onclick = function() {
    if (oMenu.style.display == 'none') {
      oMenu.style.display = 'block';
      oIcon.style.display = 'block';
      _tpi_isUserInfoOpen = true;
    } else {
      oMenu.style.display = 'none';
      oIcon.style.display = 'none';
      _tpi_isUserInfoOpen = false;
    }
  }
}

function _tpi_toggleMoreLinesModal(show) {
  if (show) {
    $('#tpi-morelines-modal').show();
  } else {
    $('#tpi-morelines-modal').hide();
  }
}

function _tpi_toggleTimeoutModal(show) {
  _tpi_timeoutCountdownActive = show;
  if (show) {
    $('#tpi-timeout-modal').show();
  } else {
    $('#tpi-timeout-modal').hide();
  }
}

function _tpi_form_logoff() {
  var oForm = document.getElementById("tpi-logout-form");
  oForm[0].value = oTPInjector.config.urlLogout == "" ? _tpi_location.href : oTPInjector.config.urlLogout;
  oForm.submit();
}

function _tpi_form_relogin(sLine) {
  var oForm = document.getElementById("tpi-relogin-form");
  oForm[0].value = sLine;
  oForm[1].value = oTPInjector.config.urlRelogin == "" ? _tpi_location.href : oTPInjector.config.urlRelogin;
  oForm[2].value = _tpi_location.href;
  oForm.submit();
}

function _tpi_showMsg(sMsg, iMsgType) {
  var hasMsg = sMsg != '' && typeof sMsg !== 'undefined';
  if (!hasMsg)
    return

  iMsgType = iMsgType || parseInt(oTPInjector.getUrlVar('tpi-msgtype'));
  var hasMsgType = iMsgType != '' && 'undefined' !== typeof iMsgType && !isNaN(iMsgType);
  var sMsgClass = "tpi-msg-info";
  if (hasMsgType) {
    switch (iMsgType) {
      case 1:
      sMsgClass = "tpi-msg-success";
      break;
      case 2:
      sMsgClass = "tpi-msg-error";
      break;
      case 3:
      sMsgClass = "tpi-msg-warning";
      break;
      default:
      sMsgClass = "tpi-msg-info";
    }
  }

  _tpi_resetMsg();
  $('.tpi-msg-text').html(decodeURIComponent(sMsg));
  $('.tpi-msg').addClass(sMsgClass).show();
  $('.tpi-msg-close').on('click', function() {
    _tpi_resetMsg();
  });
}

function _tpi_resetMsg() {
  $('.tpi-msg-text').html('');
  $('.tpi-msg').removeClass().addClass('tpi-msg').hide();
}

function _tpi_initTimeoutChat(bActive) {
  if (!bActive) return;
  if (oTPInjector.user.login) {
    if (oTPInjector.config.appName == 'AYUDA') {
      _tpi_isChatTimerActive = true;
      _tpi_timeoutChat = 2;
      _tpi_chatTimer = setInterval(_tpi_checkChatTime, 60000);
      return true;
    }
    if (oTPInjector.config.appName == 'MICUENTA') {
      if (oTPInjector.user.segmento != "110" && oTPInjector.user.segmento != "99999" && oTPInjector.user.subsegmento != "224" && oTPInjector.user.subsegmento != "226" && oTPInjector.user.subsegmento != "282") {
        _tpi_isChatTimerActive = false;
        _tpi_showChat();
        return true;
      }
    }
  }
  if (oTPInjector.config.appName == 'TIENDA') {
    if (oTPInjector.config.appSection == 'BLACK' || oTPInjector.config.appSection == 'HOME' || oTPInjector.config.appSection == 'PLANES') {
      _tpi_isChatTimerActive = true;
      _tpi_timeoutChat = 2;
      _tpi_chatTimer = setInterval(_tpi_checkChatTime, 60000);
      return true;
    }
  }
  oTPInjector.config.hasChat = false;
}
function _tpi_initTimeoutLogin(bActive) {
  if (!oTPInjector.user.login) return;
  if (!bActive) return;
  _tpi_timeoutLogin = parseInt(_tpi_getCookie("tpinjector_timeout")) * 60000;
  _tpi_timeoutCountdownMax = parseInt(_tpi_getCookie("tpinjector_countdown"));
  _tpi_isIdleTimerActive = true
  _tpi_idleTimer = setInterval(_tpi_checkIdleTime, 1000);
}

function _tpi_checkChatTime() {
  if (oTPInjector.config.hasChat) {
    _tpi_chatTime = _tpi_chatTime + 1;
    if (_tpi_chatTime >= _tpi_timeoutChat) {
      _tpi_showChat();
      clearInterval(_tpi_chatTimer);
      _tpi_isChatTimerActive = false;
      oTPInjector.config.hasChat = false;
    }
  }  
}
function _tpi_checkIdleTime() {
  if (oTPInjector.config.company == "oficinacomercial") {
    _tpi_idleTime = _tpi_idleTime + 1000;
    if (_tpi_idleTime >= _tpi_timeoutLogin) {
      _tpi_toggleTimeoutModal(true);
      _tpi_countdownHandler();
      if (_tpi_timeoutCountdown >= _tpi_timeoutCountdownMax) {
        _tpi_timeoutHandler(false);
      }
    }
  }
}
function _tpi_resetIdleTime() {
  if (_tpi_isIdleTimerActive || _tpi_isChatTimerActive) {
    _tpi_timeoutCountdown = 0;
    _tpi_idleTime = 0;
  }
}
function _tpi_timeoutHandler(bOption) {
  if (bOption) {
    _tpi_resetIdleTime();
    _tpi_toggleTimeoutModal(false);
  } else {
    clearInterval(_tpi_idleTimer);
    _tpi_isIdleTimerActive = false;
    _tpi_toggleTimeoutModal(false);
    _tpi_form_logoff();
  }
}
function _tpi_countdownHandler() {
  _tpi_timeoutCountdown++;
  var iCount = _tpi_timeoutCountdownMax - _tpi_timeoutCountdown;
  $('#tpi-timeout-modal-countdown').html(iCount);
}

function _tpi_showChat() {
  var oElement = document.getElementById('tpi-btn-chat');
  if (oElement == null) {
    oElement = document.createElement('a');
    oElement.id = 'tpi-btn-chat';
    oElement.href = "https://autogestion.personal.com.ar/Individuos/chat.aspx";
    var oEleImage = document.createElement('img');
    oEleImage.src = _tpi_imgurl + 'icons/icon-chat.png';
    oElement.appendChild(oEleImage);
    document.body.insertBefore(oElement, null);
  }
  oElement.style.display = "block";
}

function _tpi_chanchullos(isOnload) {
  var sHost = _tpi_location.hostname;
  var sPathName = _tpi_location.pathname;
  
  //********************************
  // Al Finalizar Carga de Pagina
  if (isOnload) {

    if (_tpi_location.hostname == 'personal.andreani.com') {
      _tpi_statsComScore('tienda.seguimiento_andreani');
      _tpi_statsGTM(window, document, 'script', 'dataLayer', 'GTM-KX5M');
    }

    if (_tpi_location.hostname.startsWith('club')) {
      $('#contenedor').css('padding-top', '10px');
      $('#panelRight_i').css('margin-top', '10px');
      var oLoginForm = document.getElementById('panelRight_i');
      if (oLoginForm) {
        oLoginForm.innerHTML = '';
      }
      var oLogedinForm = document.getElementById('panelRight_login');
      if (oLogedinForm) {
        var sUserName = _tpi_funcCapitalise($('#panelRight_login h2').eq(0).text());
        $('#panelRight_login h1 span').remove();
        $('.tpi-user').text(sUserName);
        $('.tpi-username strong').text(sUserName);
        $('#panelRight_login h2').eq(0).remove();
        $('#panelRight_login div.tel_user h2').remove();
      }
    }

  // ********************************
  // Al Finalizar Carga de TPInjector
  } else {

    //set de métricas de usuario
    if (oTPInjector.user.line != '') {
      var mData = "&active_service=" + oTPInjector.user.line + "&segmento=" + oTPInjector.user.segmento + "&subsegmento=" + oTPInjector.user.subsegmento;
      if (typeof g_comScore == 'function') { 
        g_comScore(mData);
        } else {
          if (typeof comScore == 'function') {
            var t = 'http' + (_tpi_location.href.charAt(4) == 's' ? 's://sb' : '://b') + '.scorecardresearch.com/p?c1=2&c2=6906383&ns_site=personal&name=';
            comScore(t + mData);
        }
      }
    }
    //fin de pruebas de metricas
    // Set metrica oficinas comerciales
    if (oTPInjector.config.company == "oficinacomercial") {
      _tpi_statsComScore(oTPInjector.config.oficina);
    }

    //Fix para links de AngularJS
    $(".tpi-navbar-inner a").click(function() {
      $('.tpi-navbar-item1-bg').hide();
      $('.tpi-navbar-item2-bg').hide();
      $('.tpi-navbar-item3-bg').hide();
      $('.tpi-navbar-item4-bg').hide();
      _tpi_toggleSuperMenu(null, false);
    });

     // Muestra el boton de chat en mi cuenta
    var sPathFirst = _tpi_location.pathname.split('/');
   if (sPathFirst[1] == "individuos" || sPathFirst[1] == "Individuos") {
      //validación de segmento: no se muestra el boton de chat para:
      if(oTPInjector.user.subsegmento != "224" && oTPInjector.user.subsegmento != "226" && oTPInjector.user.subsegmento != "282" && oTPInjector.user.segmento != "110" && oTPInjector.user.segmento != "99999"){
        var oDiv = document.getElementById('aspnetForm');
        var oElement = document.createElement('div');
        oElement.id = 'tpi_chat_icon';
        var oEleAnchor = document.createElement('a');
        oEleAnchor.href = "https://autogestion.personal.com.ar/Individuos/chat.aspx";
        var oEleImage = document.createElement('img');
        oEleImage.id = 'tpi_chat_img';
        oEleImage.src = _tpi_imgurl + 'icons/ico-chat.png';
        var oEleText = document.createElement('p');
        oEleText.innerHTML = "Chat con representantes";
        oEleAnchor.appendChild(oEleImage);
        oEleAnchor.appendChild(oEleText);
        oElement.appendChild(oEleAnchor);
        _tpi_insertAfter(oElement, oDiv);
      }
      if (sPathFirst[2] == "ConfiguracionTerminales.aspx") {
        var o4GDiv = $('#DM').next();
        if (o4GDiv) {
          o4GDiv.html('<div style="text-align: center; color: #aaa; font-size: 16px">¿Querés configurar tu equipo 4G?&nbsp;<a href="http://www.personal.com.ar/4g/#configuracion">Hacelo ahora &gt;</a></div>');
        }
      }
    }

  }

  _tpi_stamp_end = new Date().getTime();
  oTPInjector.env.loadtime = _tpi_stamp_end - _tpi_stamp;
}

function _tpi_winloadCallback() {

}


/*
>>>>>>>>>>> FIN DE CODIGO NORMALIZADO <<<<<<<<<<<
-------------------------------------------------
>>>>>>>>>>>>>>>>>>>>> A REVISAR DE AK EN ADELANTE
*/
function _tpi_setPositions() {
  var iBodyHeight = 0;
  var oBody = document.body;
  var oDocEle = document.documentElement;
  var oHeader = document.getElementById('_injector_header');
  var oFooter = document.getElementById('_injector_footer');
  oBody.style.marginTop = '70px';
  oBody.style.paddingTop = '0px';
  oBody.style.marginBottom = oFooter.offsetHeight + 'px';
  if (typeof document.height !== 'undefined') {
    iBodyHeight = document.height;
  } else {
    iBodyHeight = Math.max(oBody.scrollHeight, oBody.offsetHeight, oDocEle.clientHeight, oDocEle.scrollHeight, oDocEle.offsetHeight);
  }
  oFooter.style.top = (iBodyHeight - oFooter.offsetHeight) + 'px';
}

//prueba de animacion

function animate(opts) {
  var start = new Date
  var id = setInterval(function() {
    var timePassed = new Date - start
    var progress = timePassed / opts.duration

    if (progress > 1) progress = 1

      var delta = opts.delta(progress)
    opts.step(delta)

    if (progress == 1) {
      clearInterval(id)
    }
  }, opts.delay || 10)
}

function move(element, delta, duration, to) {
  animate({
    delay: 10,
    duration: duration || 1000, // 1 sec by default
    delta: delta,
    step: function(delta) {
      element.style.left = to*delta + "px"    
    }
  })
}

function linear(progress) {
  return progress
}

function circ(progress) {
  return 1 - Math.asin(Math.asin(progress))
}

function circInv(progress){
 return Math.sin(progress)
}

function back(progress, x) {
  return Math.pow(progress, 2) * ((x + 1) * progress - x)
}

function makeEaseOut(delta) {
  return function(progress) {
    return 1 - delta(1 - progress)
  }
}

var circEaseOut = makeEaseOut(circ)
var circInvEaseOut = makeEaseOut(circInv)

//Browser detection

function _tpi_getInternetExplorerVersion() {
  var rv = -1; // Return value assumes failure.
  if (navigator.appName == 'Microsoft Internet Explorer') {
    var ua = navigator.userAgent;
    var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
    if (re.exec(ua) != null)
      rv = parseFloat(RegExp.$1);
  }
  return rv;
}

function _tpi_checkIEVersion() {
  var msg = "You're not using Windows Internet Explorer.";
  var ver = _tpi_getInternetExplorerVersion();
  if (ver > -1) {
    if (ver >= 9.0){
      msg = "You're using a recent copy of Windows Internet Explorer."
      return false;
    } else {
      msg = "You should upgrade your copy of Windows Internet Explorer.";
      return true;
    }
  }
}